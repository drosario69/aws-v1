package com.lavacro.cloud.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;

import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
/*
import java.util.Set;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
*/
import java.util.*;

class WorkFlow {
	private static final String SECURITY_GROUP = "JavaSecurityGroup";
	private static final List<String> IPS = Arrays.asList("127.0.0.1/32", "98.13.162.40/32");
	private AmazonEC2 ec2;

	private List<String> steps = Arrays.asList("getEC2", "createSecurityGroup",
			"setIP"/*, "makeKeys", "runEC2"*/);

	@SuppressWarnings("unused")
	private boolean getEC2() {
		AWSCredentials creds = new BasicAWSCredentials("AKIAJSVXCNXF3H44VTOQ", "OeVAtZWoR+sQ9UzS1K7ji7K1Wi2uGZOWvTm9WsGX");
		AWSCredentialsProvider credentials = new AWSStaticCredentialsProvider(creds);

		try {
			ec2 = AmazonEC2ClientBuilder
					.standard()
					.withCredentials(credentials)
					.withRegion(Regions.US_EAST_1)
					.build();
			return true;
		} catch(AmazonEC2Exception e) {
			System.err.println("Cannot create EC2 service: " + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unused")
	private boolean createSecurityGroup() {
		DescribeSecurityGroupsResult res = ec2.describeSecurityGroups();
		Optional<SecurityGroup> secgrp = res.getSecurityGroups().stream().filter(sg -> sg.getGroupName().equals(SECURITY_GROUP)).findAny();
		if(secgrp.isPresent()) {
			System.out.println("Security group already exists");
			return true;
		}

		CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest()
				.withGroupName(SECURITY_GROUP)
				.withDescription("test security group");

		try {
			ec2.createSecurityGroup(csgr);
		} catch (AmazonEC2Exception e) {
			System.err.println("Cannot create security group: " + e.getMessage());
			return false;
		}
		return true;
	}

	@SuppressWarnings("unused")
	private boolean setIP() {
		// by this point, the security group must exist
		DescribeSecurityGroupsRequest req = new DescribeSecurityGroupsRequest().withGroupNames(SECURITY_GROUP);
		DescribeSecurityGroupsResult res = ec2.describeSecurityGroups(req);
		SecurityGroup secgrp = res.getSecurityGroups().get(0);
		List<String> currIP = new ArrayList<>();
		for(IpPermission ip: secgrp.getIpPermissions()) {
			for(IpRange range: ip.getIpv4Ranges()) {
				currIP.add(range.getCidrIp());
			}
		}

		List<IpRange> outstanding = new ArrayList<>();
		for(String myIP: IPS) {
			if(! currIP.contains(myIP)) {
				outstanding.add(new IpRange().withCidrIp(myIP));
			}
		}

		if(outstanding.size() > 0) {
			System.out.println("Outstanding: " + outstanding.size());
			IpPermission ipPermission = new IpPermission();
			ipPermission.withIpv4Ranges(outstanding)
				.withFromPort(22)
				.withToPort(22)
				.withIpProtocol("tcp");

			AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest()
					.withGroupName(SECURITY_GROUP)
					.withIpPermissions(ipPermission);
			try {
				ec2.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
			} catch (AmazonEC2Exception e) {
				System.err.println("Cannot set IP permissions: " + e.getMessage());
				return false;
			}
		} else {
			System.out.println("Security group permissions already set");
		}
		return true;
	}

	@SuppressWarnings("unused")
	private void makeKeys() {
		CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest();
		createKeyPairRequest.withKeyName("secretKeyName");

		try {
			CreateKeyPairResult createKeyPairResult = ec2.createKeyPair(createKeyPairRequest);

			KeyPair keyPair = createKeyPairResult.getKeyPair();
			String privateKey = keyPair.getKeyMaterial();
			try {
				FileOutputStream fos = new FileOutputStream("/home/david/secretKeyName.pem");
				fos.write(privateKey.getBytes());
				fos.close();

				/*Set<PosixFilePermission> perm = PosixFilePermissions.fromString("r--------");
				FileAttribute<?> permissions = PosixFilePermissions.asFileAttribute(perm);
				Files.createFile(path, permissions);*/
			} catch (Exception e) {
				System.err.println("Bad news: " + e.getMessage());
			}
		} catch(AmazonEC2Exception e) {
			System.err.println("Cannot create keypair: " + e.getMessage());
		}
	}

	@SuppressWarnings("unused")
	private void runEC2() {
		RunInstancesRequest runInstancesRequest =
				new RunInstancesRequest()
						.withImageId("ami-0cc96feef8c6bbff3")
						.withInstanceType(InstanceType.T1Micro)
						.withMinCount(1)
						.withMaxCount(1)
						.withKeyName("secretKeyName")
						.withSecurityGroups(SECURITY_GROUP);
		//RunInstancesResult result =
		ec2.runInstances(runInstancesRequest);
	}

	//****** rollback methods *******
	@SuppressWarnings("unused")
	private void deleteSecurityGroup() {
		DeleteSecurityGroupRequest req = new DeleteSecurityGroupRequest()
				.withGroupName(SECURITY_GROUP);
		try {
			ec2.deleteSecurityGroup(req);
		} catch(AmazonEC2Exception e) {
			System.err.println("Error deleting security group: " + e.getMessage());
		}
	}

	void build() {
		for (String step : steps) {
			// does a method by that name exist?
			try {
				Method method = this.getClass().getDeclaredMethod(step);
				System.out.println("Running: " + step);
				boolean ok = (boolean) method.invoke(this);
				if(!ok) {
					System.err.println("Aborting");
					break;
				}
			} catch(NoSuchMethodException e) {
				System.err.println("Method does NOT exist: " + step);
				break;
			} catch(IllegalAccessException e) {
				System.err.println("Illegal access exception: " + e.getMessage());
				break;
			} catch(InvocationTargetException e) {
				System.err.println("Invocation target exception: " + e.getMessage());
				break;
			}
		}
		ec2.shutdown();
	}
}
