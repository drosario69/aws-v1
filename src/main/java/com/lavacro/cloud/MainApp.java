package com.lavacro.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;

@ComponentScan
@SpringBootApplication
public class MainApp {
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(MainApp.class, args);

		long before = new Date().getTime();
		Test test = ctx.getBean(Test.class);
		test.foobar();
		long after = new Date().getTime();

		System.out.println("1.x API = " + (after - before) + " ms");
		System.out.println("****************************************************");
	}
}
