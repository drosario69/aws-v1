package com.lavacro.cloud.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "s3")
public class S3props {
	private String accessKey;
	private String secretKey;
	private String endpoint;
	private String uri;
	private String region;

	public String getAccessKey() { return accessKey; }
	public void setAccessKey(String accessKey) { this.accessKey = accessKey; }

	public String getSecretKey() { return secretKey; }
	public void setSecretKey(String secretKey) { this.secretKey = secretKey; }

	public String getEndpoint() { return endpoint; }
	public void setEndpoint(String endpoint) { this.endpoint = endpoint; }

	public String getRegion() { return region; }
	public void setRegion(String region) { this.region = region; }

	public String getUri() { return uri; }
	public void setUri(String uri) { this.uri = uri; }

	@Override
	public String toString() {
		return "S3props{" +
				"accessKey='" + accessKey + '\'' +
				", secretKey='" + secretKey + '\'' +
				", endpoint='" + endpoint + '\'' +
				", region='" + region + '\'' +
				'}';
	}
}
