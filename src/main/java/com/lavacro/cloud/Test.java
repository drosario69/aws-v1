package com.lavacro.cloud;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.lavacro.cloud.clients.S3client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Test {
	private S3client s3client;

	@Autowired
	Test(S3client s3client) {
		this.s3client = s3client;
		System.out.println(s3client);
	}

	public void foobar() {
		AmazonS3 s3 = s3client.getS3Client();
		s3.listBuckets().forEach( bucket -> {
			System.out.println(bucket.getName());
			ListObjectsRequest req = new ListObjectsRequest()
					.withBucketName(bucket.getName());
			ObjectListing res = s3.listObjects(req);
			List<S3ObjectSummary> objList = res.getObjectSummaries();
			for(S3ObjectSummary obj: objList) {
				System.out.println(String.format(
						"\tObject: %s, size: %d, owner: %s, id: %s",
						obj.getKey(), obj.getSize(),
						obj.getOwner().getDisplayName(),
						obj.getOwner().getId()));
			}
		});
		s3.shutdown();
	}

}
