package com.lavacro.cloud.clients;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.lavacro.cloud.properties.S3props;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class S3client {
	private S3props s3props;

	@Autowired
	S3client(S3props s3props) {
		this.s3props = s3props;
	}

	public AmazonS3 getS3Client() {
		System.out.println(s3props.toString());
		AWSCredentials creds = new BasicAWSCredentials(s3props.getAccessKey(), s3props.getSecretKey());
		AWSCredentialsProvider credentials = new AWSStaticCredentialsProvider(creds);
		ClientConfiguration config = new ClientConfiguration();
		AmazonS3 s3 = AmazonS3ClientBuilder.standard()
				.withClientConfiguration(config)
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(s3props.getEndpoint(), s3props.getRegion()))
				.withCredentials(credentials)
				.build();

		System.out.println("Created S3 object");
		return s3;
	}
}
